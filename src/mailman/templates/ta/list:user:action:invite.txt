உங்கள் முகவரி "$ USER_EMAIL" $ short_listname இல் சேர அழைக்கப்பட்டுள்ளது
 $ shord_listname அஞ்சல் பட்டியல் உரிமையாளர் மூலம் $ டொமைனில் அஞ்சல் பட்டியல்.
 இந்த செய்திக்கு வெறுமனே பதிலளிப்பதன் மூலம் நீங்கள் அழைப்பை ஏற்கலாம்.

 அல்லது நீங்கள் பின்வரும் வரியை சேர்க்க வேண்டும் - பின்வருவனவற்றை மட்டுமே
 வரி - $ request_email க்கு ஒரு செய்தியில்:

 உறுதிப்படுத்தவும் $ டோக்கன்

 இந்த செய்திக்கு ஒரு `பதிலை 'அனுப்புவது வேலை செய்ய வேண்டும் என்பதை நினைவில் கொள்க
 பெரும்பாலான அஞ்சல் வாசகர்கள்.

 இந்த அழைப்பை நீங்கள் மறுக்க விரும்பினால், தயவுசெய்து இதை புறக்கணிக்கவும்
 செய்தி. உங்களிடம் ஏதேனும் கேள்விகள் இருந்தால், தயவுசெய்து அவற்றை அனுப்புங்கள்
 $ உரிமையாளர்_மெயில்.
